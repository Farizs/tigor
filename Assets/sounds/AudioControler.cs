﻿using UnityEngine;
using System.Collections;

public class AudioControler : MonoBehaviour {

    public AudioClip archer;
    public AudioClip gameplay;
    public AudioClip getitemeffect;
    public AudioClip knifeeffect;
    public AudioClip maceeffect;
    public AudioClip swordeffect;
    public AudioClip dieSound;
}
