﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour {
    int jumlahpukul;
    Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
    public void pukul(){
        jumlahpukul++;
        if (jumlahpukul>=4){
            StartCoroutine(pusing());
        }
    }
    IEnumerator pusing(){
        anim.SetBool("Pusing", true);
        yield return new WaitForSeconds(8);
        jumlahpukul=0;
        anim.SetBool("Pusing", false);
    }
}
