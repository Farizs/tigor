﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public Animator anim;
    public GameObject Objectmove;
    public GameObject TargetMove;
    public float speed;
    public float delay;
    float step;
    public float StartDelay;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(MoveObject());
    }

    // Update is called once per frame
    void Update()
    {
        step = speed * Time.deltaTime;

    }
    public IEnumerator MoveObject()
    {
        bool movetotarget = true;
        yield return new WaitForSeconds(StartDelay);
        while (true)
        {
            if (movetotarget == true)
            {
                Objectmove.transform.position = Vector2.MoveTowards(Objectmove.transform.position, TargetMove.transform.position, step);

                if (Objectmove.transform.position == TargetMove.transform.position)
                {
                    if(anim!=null)
                    anim.SetBool("target", true);
                    yield return new WaitForSeconds(delay);
                    movetotarget = false;
                }
            }
            if (movetotarget == false)
            {
                Objectmove.transform.position = Vector2.MoveTowards(Objectmove.transform.position, gameObject.transform.position, step);
                if (Objectmove.transform.position == gameObject.transform.position)
                {
                    if (anim != null)
                    anim.SetBool("target", false);
                    yield return new WaitForSeconds(delay);
                    movetotarget = true;
                }
            }
            yield return new WaitForSeconds(.01f);
        }
    }
}