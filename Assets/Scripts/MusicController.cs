﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    public float time;
    public AudioSource bgmMusic;
    public AudioSource clickMusic;
    public float musicVolume;
    public float klikVolume;
    
    public static MusicController instance;
    // Use this for initialization
   
    private void Start()
    {
        AudioSource[] audioSource = GetComponents<AudioSource>();

        bgmMusic = audioSource[0];
        clickMusic = audioSource[1];

        musicVolume = PlayerPrefs.GetFloat("SliderBGM value");
        SetMusicVolumeBGM(musicVolume);

        klikVolume = PlayerPrefs.GetFloat("SliderSFX value");
        SetMusicVolumeSFX(klikVolume);

        PlayOrTurnOfMusic(musicVolume);
        PlayOrTurnOf(klikVolume);
        MusicManager();
    }
    public void SetMusicVolumeBGM(float vol)
    {
        PlayOrTurnOfMusic(vol);
    }

    public void SetMusicVolumeSFX(float vol)
    {
        PlayOrTurnOf(vol);
    }

    void PlayOrTurnOf(float vol)
    {
        klikVolume = vol;
        clickMusic.volume = klikVolume;
        if (clickMusic.volume > 0)
        {
            if (!clickMusic.isPlaying)
            {
                clickMusic.Stop();
            }
        }
        else if (clickMusic.volume == 0)
        {
            if (!clickMusic.isPlaying)
            {
                clickMusic.Stop();
            }
        }
    }

    void PlayOrTurnOfMusic(float vol)
    {
        musicVolume = vol;
        bgmMusic.volume = musicVolume;
        

        if (bgmMusic.volume > 0)
        {
            if (!bgmMusic.isPlaying)
            {
                bgmMusic.Play();
            }
        }
        else if (bgmMusic.volume == 0)
        {
            if (!bgmMusic.isPlaying)
            {
                bgmMusic.Stop();
            }
        }

       

    }

    void MusicManager()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void GameIsLoadedTurnOfMusic()
    {
        if (bgmMusic.isPlaying)
        {
            time = bgmMusic.time;
            bgmMusic.Stop();
        }
    }

    public void PlayBgmMusic()
    {
        if (!bgmMusic.isPlaying)
        {
            bgmMusic.Play();
        }
    }
    public void StopBgmMusic()
    {

        if (!bgmMusic.isPlaying)
        {
            bgmMusic.Stop();
        }
    }
    public void PlayClickClip()
    {
        clickMusic.Play();
    }
    public static MusicController GetInstance()
    {
        return instance;
    }
}