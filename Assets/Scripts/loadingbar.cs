﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class loadingbar : MonoBehaviour {
	AsyncOperation ao;
	public GameObject loadingscreenbg;
	public Slider progBar;
	public Text loadingText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public IEnumerator LoadLevelWithRealProgress(string scenename){
		{print (scenename);
			loadingscreenbg.SetActive (true);
			yield return new WaitForSecondsRealtime (1);
			ao = SceneManager.LoadSceneAsync (scenename);
			ao.allowSceneActivation = false;
			print (ao.progress);

			while (!ao.isDone) {
				progBar.value = ao.progress;
				float loadingvalue = (int)(ao.progress * 100);
				loadingText.text = "Loading "+ loadingvalue.ToString () + "%";
				if (ao.progress == 0.9f) {
					ao.allowSceneActivation = true;
				}
				//Debug.Log (ao.progress);
				yield return null;
			}
		}
	}
}
