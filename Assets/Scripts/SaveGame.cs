﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveGame : MonoBehaviour {
	public static SaveGame instance;
	public static int lastlevel;
	// Use this for initialization
	void Awake(){
		if (instance == null)
			instance = this;
		}
	void Start () {
		LoadPlayerData ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void SavePlayerData()
	{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");

		playerData data = new playerData ();
		print (lastlevel);
		data.lastlevel = lastlevel;
		bf.Serialize( file, data);
			file.Close();
		}

	public void LoadPlayerData()
	{
		playerData data = new playerData();
		if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			data = (playerData)bf.Deserialize(file);
			file.Close();
			print (data.lastlevel);
			lastlevel = data.lastlevel;
		}
	}
	}
[Serializable]
class playerData
{
	public int lastlevel;
}
