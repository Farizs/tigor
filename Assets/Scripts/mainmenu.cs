﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TeamUtility.IO.Examples;
public class mainmenu : MonoBehaviour {
    [Header("HUD")]
	public GameObject NewGame;
	public GameObject Continue;
	public GameObject LoadGame;
	public GameObject Extra;
	public GameObject Option;
	public GameObject QuittoDesktop;
	
    [Header("Options HUD")]
    public GameObject video;
    public GameObject sound;
    public GameObject soundmenu;
    public GameObject SFX;
	public float sfxvalue;
	public Slider sfxslider;
	public float bgmvalue;
	public Slider bgmslider;
	public GameObject BGM;
	public GameObject controller;
	public GameObject controllermenu;
	public GameObject UP;
	public GameObject keyboard;
    public GameObject keyboardMenu;
    public GameObject language;
    public GameObject languageMenu;
    public GameObject exit;
	public GameObject Optionmenu;
	public GameObject resolutionmenu;
	public GameObject resolution;
	public GameObject resolution_seledcted;
	public Text resolutiontext;
	public Text fullscreentext;
	public GameObject fullscreen;
	public bool fullscreenbool;
	public Vector2[] resolutionlist;
	public Resolution[] resolutionarray;

    [Header("Controll Music")]
    [SerializeField]
    public MusicController musicController;
    public Slider sliderSFX;
    public Slider sliderBGM;
    
    public float clickVolume;
    public float musicVolume;

    [Header("Save")]
	public string menuselected;
	public static int resindex = 10;
	public GameObject savekey;
	public GameObject loadkey;

	// Use this for initialization
	void Start () {
        musicController = (MusicController)FindObjectOfType(typeof(MusicController));
        sliderSFX.value = PlayerPrefs.GetFloat("SliderSFX value");
        sliderBGM.value = PlayerPrefs.GetFloat("SliderBGM value");


        Invoke("selectedbutton",3);
		resolutionarray = Screen.resolutions;
		foreach (Resolution res in resolutionarray) {
			print(res);
		}
		if (resindex == 10)
			resindex = 5;
		Screen.SetResolution((int)resolutionlist[resindex].x, (int)resolutionlist[resindex].y, true);
		print(Screen.width);

        
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Return)) {
			if (EventSystem.current.currentSelectedGameObject == NewGame) {
				StartCoroutine (gameObject.GetComponent<loadingbar> ().LoadLevelWithRealProgress ("level 1"));
				MoreMountains.CorgiEngine.LevelManager.currentlevel = 0;
			}
			else if (EventSystem.current.currentSelectedGameObject == Continue) {
				MoreMountains.CorgiEngine.LevelManager.currentlevel = SaveGame.lastlevel;
				print (MoreMountains.CorgiEngine.LevelManager.currentlevel);
				StartCoroutine (gameObject.GetComponent<loadingbar> ().LoadLevelWithRealProgress ("level 1"));
			}
			else if (EventSystem.current.currentSelectedGameObject == LoadGame)
				StartCoroutine (gameObject.GetComponent<loadingbar> ().LoadLevelWithRealProgress ("level 1"));	
//			else if (EventSystem.current.currentSelectedGameObject == Extra)
//				SceneManager.LoadScene ("level 1");
			else if (EventSystem.current.currentSelectedGameObject == Option)
				selectedbuttonOPTION ();
			else if (EventSystem.current.currentSelectedGameObject == video)
				selectedbuttonVIDEO ();
			else if (EventSystem.current.currentSelectedGameObject == resolution)
				selectedbuttonVIDEO ();
			else if (EventSystem.current.currentSelectedGameObject == controller)
				selectedbuttonCONTROLLER ();
            else if (EventSystem.current.currentSelectedGameObject == keyboard)
                selectedbuttonKEYBOARD();
            else if (EventSystem.current.currentSelectedGameObject == language)
                selectedbuttonLANGUAGE();
            else if (EventSystem.current.currentSelectedGameObject.tag == "keybutton")
				keybinding ();
			else if (EventSystem.current.currentSelectedGameObject == sound)
				selectedbuttonsound ();
			else if (EventSystem.current.currentSelectedGameObject == exit) {
				selectedbuttonEXIT ();
			} else if (EventSystem.current.currentSelectedGameObject == QuittoDesktop)
				selectedbuttonQUIT ();
		}
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			if (EventSystem.current.currentSelectedGameObject == fullscreen)
				selectedfullscreen ();
			if (EventSystem.current.currentSelectedGameObject == resolution) {
				leftres ();
			}
			if (EventSystem.current.currentSelectedGameObject == SFX) {
				sfxslider.value -= 0.1f;
				sfxcontrol ();
			}
			if (EventSystem.current.currentSelectedGameObject == BGM) {
				bgmslider.value -= 0.1f;
				bgmcontrol ();
			}
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			if (EventSystem.current.currentSelectedGameObject == fullscreen)
				selectedfullscreen ();
			if (EventSystem.current.currentSelectedGameObject == resolution) {
				rightres ();
			}
			if (EventSystem.current.currentSelectedGameObject == SFX) {
				sfxslider.value += 0.1f;
				sfxcontrol ();
			}
			if (EventSystem.current.currentSelectedGameObject == BGM) {
				bgmslider.value += 0.1f;
				bgmcontrol ();
			}
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			selectedbuttonEXIT ();
		}
        PlayerPrefs.SetFloat("SliderSFX value", sliderSFX.value);
        PlayerPrefs.SetFloat("SliderBGM value", sliderBGM.value);

    }
    public void play(){

        MusicController.instance.PlayClickClip();
        StartCoroutine (gameObject.GetComponent<loadingbar> ().LoadLevelWithRealProgress ("level 1"));	
		MoreMountains.CorgiEngine.LevelManager.currentlevel = 0;

	}

	public void continue_play(){
        MusicController.instance.PlayClickClip();
        MoreMountains.CorgiEngine.LevelManager.currentlevel = SaveGame.lastlevel;
		StartCoroutine (gameObject.GetComponent<loadingbar> ().LoadLevelWithRealProgress ("level 1"));	
	}
		
	void selectedbutton(){
        //MusicController.instance.PlayBgmMusic();
        EventSystem.current.SetSelectedGameObject (NewGame);
	}
	public void leftres(){
		print (resindex);
		if (resindex > 0) {
			resindex--;
			Screen.SetResolution ((int)resolutionlist[resindex].x,(int)resolutionlist[resindex].y, Screen.fullScreen);
			resolutiontext.text = Screen.width + " x " +Screen.height;
		}
	}
	public void rightres(){
		if (resindex < resolutionarray.Length-2) {
			resindex++;
			Screen.SetResolution ((int)resolutionlist[resindex].x, (int)resolutionlist[resindex].y, Screen.fullScreen);
			resolutiontext.text = Screen.width + " x " + Screen.height;
		}
	}
	public void bgmcontrol(){
		bgmvalue = bgmslider.value;
	}
	public void sfxcontrol(){
		sfxvalue = sfxslider.value;
	}
	public void selectedbuttonOPTION(){
        MusicController.instance.PlayClickClip();
        menuselected = "option";
		Optionmenu.SetActive (true);
		EventSystem.current.SetSelectedGameObject (video);
        
    }

    public void selectedbuttonEXTRA()
    {
        MusicController.instance.PlayClickClip();
        menuselected = "extra";
        Optionmenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(video);
        MusicController.instance.PlayClickClip();
    }
    public void selectedbuttonCONTROLLER(){
        MusicController.instance.PlayClickClip();
        if (menuselected != "option")
			selectedbuttonEXIT ();
		menuselected = "controller";
		controllermenu.SetActive (true);
		EventSystem.current.SetSelectedGameObject (UP);
	}
    public void selectedbuttonKEYBOARD()
    {
        MusicController.instance.PlayClickClip();
        if (menuselected != "option")
            selectedbuttonEXIT();
        menuselected = "keyboard";
        keyboardMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(UP);
    }

    public void selectedbuttonLANGUAGE()
    {
        MusicController.instance.PlayClickClip();
        if (menuselected != "option")
            selectedbuttonEXIT();
        menuselected = "language";
        languageMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(UP);
    }
    public void selectedbuttonVIDEO(){
        MusicController.instance.PlayClickClip();
        if (menuselected != "option")
		selectedbuttonEXIT ();
		menuselected = "video";
		resolutiontext.text = Screen.width + " x " +Screen.height;
		resolutionmenu.SetActive (true);
		EventSystem.current.SetSelectedGameObject (resolution);
	}
	public void selectedbuttonResolution(){
		menuselected = "resolution";
	}
	public void selectedfullscreen(){
		if (Screen.fullScreen) {
			fullscreentext.text = "OFF";
			Screen.fullScreen = false;
		} else {
			fullscreentext.text = "ON";
			Screen.fullScreen = true;
		}
	}
	public void keybinding(){
		EventSystem.current.currentSelectedGameObject.GetComponent<RebindInput> ().rebind ();
	}
	public void selectedbuttonsound(){
        MusicController.instance.PlayClickClip();
        if (menuselected != "option")
		selectedbuttonEXIT ();
		menuselected = "sound";
		soundmenu.SetActive (true);
		EventSystem.current.SetSelectedGameObject (SFX);
	}
	public void selectedbuttonEXIT(){
      
        if (menuselected == "option") {
            MusicController.instance.PlayClickClip();
            menuselected = "mainmenu";
			Optionmenu.SetActive (false);
			EventSystem.current.SetSelectedGameObject (NewGame);
		}
		if (menuselected == "video") {
            MusicController.instance.PlayClickClip();
            menuselected = "option";
			resolutionmenu.SetActive (false);
			EventSystem.current.SetSelectedGameObject (video);
		}
		if (menuselected == "sound") {
            MusicController.instance.PlayClickClip();
            menuselected = "option";
			soundmenu.SetActive (false);
			EventSystem.current.SetSelectedGameObject (sound);
		}
		if (menuselected == "controller") {
            MusicController.instance.PlayClickClip();
            menuselected = "option";
			savekey.GetComponent<SaveInputs> ().Save ();
			controllermenu.SetActive (false);
			EventSystem.current.SetSelectedGameObject (controller);
		}
        if (menuselected == "keyboard")
        {
            MusicController.instance.PlayClickClip();
            menuselected = "option";
            keyboardMenu.SetActive(false);
            EventSystem.current.SetSelectedGameObject(keyboard);
        }

        if (menuselected == "language")
        {
            MusicController.instance.PlayClickClip();
            menuselected = "option";
            languageMenu.SetActive(false);
            EventSystem.current.SetSelectedGameObject(language);
        }
    }
	public void selectedbuttonQUIT(){
        MusicController.instance.PlayClickClip();
        Application.Quit ();
	}

    public void GetVolumeBGM(float volume)
    {
        musicController.SetMusicVolumeBGM(volume);
        musicVolume = volume;
    }

    public void GetVolumeSFX(float volume)
    {
        musicController.SetMusicVolumeSFX(volume);
        clickVolume = volume;
    }


}
