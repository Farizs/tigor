﻿using UnityEngine;
using Steamworks;
 
public class SteamScript : MonoBehaviour {
    void Start() {
        print(SteamManager.Initialized);
        if (SteamManager.Initialized) {
            string name = SteamFriends.GetPersonaName();
            Debug.Log(name);
        }
    }
}