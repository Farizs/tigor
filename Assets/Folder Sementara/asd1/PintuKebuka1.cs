﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PintuKebuka1 : MonoBehaviour
{
    
    public bool Switch = false;
    public SwitchOn[] SwitchOnScript;

    public Animator gerak;
    public BoxCollider2D colid1;
    public UnityEvent action;

    private void Start()
    {
        gerak = GetComponent<Animator>();
        colid1 = GetComponent<BoxCollider2D>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "DmgArea")
        {
            if (SwitchOnScript.Length > 0)
            {
                foreach (SwitchOn _switch in SwitchOnScript)
                {
                    _switch.Nyala(1);
                    Debug.Log("jmlh Kurang -1 = " + _switch.switchPoint);
                }
            }
            else{
                action.Invoke();
            }
                Switch = true;
                FalseLagi();
        }
    }

    void FalseLagi()
    {
        if (Switch == true)
        {
            gerak.SetBool("switch", Switch);
            Debug.Log("switch udah true");
            Switch = false;
            colid1.enabled = false;
        }
    }   
}