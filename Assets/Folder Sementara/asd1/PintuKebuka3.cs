﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PintuKebuka3 : MonoBehaviour
{

    public bool Switch = false;

    public SwitchOn S;

    public Animator gerak;
    public BoxCollider2D colid3;


    private void Start()
    {
        gerak = gameObject.GetComponent<Animator>();
        colid3 = gameObject.GetComponent<BoxCollider2D>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "DmgArea")
        {
            S.Nyala(1);
            Debug.Log("jmlh Kurang -1 = " + S.switchPoint);
            Switch = true;
            FalseLagi();
        }
    }

    void FalseLagi()
    {
        if (Switch == true)
        {
            gerak.SetBool("switch", Switch);
            Debug.Log("switch udah true");
            Switch = false;
            colid3.enabled = false;
        }
    }
}