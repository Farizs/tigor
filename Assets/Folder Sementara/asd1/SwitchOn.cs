﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
public class SwitchOn : MonoBehaviour
{
    public bool Kebuka = false;
    public GameObject kamera;
    public Animator Angkat;
    public bool KameraAnimation;
    public int switchPoint = 3;


	private void Start()
    {
        Angkat = GetComponent<Animator>();

    }



    public void FalseLagi()
    {
        Angkat.SetBool("kebuka", true);
            Debug.Log("pintu harusnya kebuka");
    }

    public void Nyala(int on)
    {
        switchPoint -= on;
        if (switchPoint == 0)
        {
            if (KameraAnimation)
                StartCoroutine(bukapintu());
            else
            FalseLagi();
        }
    }
    IEnumerator bukapintu(){
        CameraController.instance.FollowsPlayer = false;
        while (CameraController.instance.gameObject.transform.position.x != transform.position.x && CameraController.instance.gameObject.transform.position.y != transform.position.y)
        {
            CameraController.instance.gameObject.transform.position = Vector3.MoveTowards(CameraController.instance.gameObject.transform.position, new Vector3(transform.position.x, transform.position.y, CameraController.instance.gameObject.transform.position.z), Time.deltaTime * 20);
            yield return new WaitForSeconds(0.001f);
        }
        CameraController.instance._target = gameObject.transform;
        CameraController.instance.FollowsPlayer = true;
        yield return new WaitForSeconds(3f);
        CameraController.instance.Shake(new Vector3(.2f, .5f, .2f));
        Angkat.SetBool("kebuka", true);
        yield return new WaitForSeconds(3);
        CameraController.instance.FollowsPlayer = false;
        while (CameraController.instance.gameObject.transform.position.x != LevelManager.Instance.Players[0].transform.position.x && CameraController.instance.gameObject.transform.position.y != LevelManager.Instance.Players[0].transform.position.y)
        {
            CameraController.instance.gameObject.transform.position = Vector3.MoveTowards(CameraController.instance.gameObject.transform.position, new Vector3(LevelManager.Instance.Players[0].transform.position.x, LevelManager.Instance.Players[0].transform.position.y, CameraController.instance.gameObject.transform.position.z), Time.deltaTime * 40);
            yield return new WaitForSeconds(0.001f);
            print("balik");
        }
        CameraController.instance._target = LevelManager.Instance.Players[0].transform;
        CameraController.instance.FollowsPlayer = true;
    }
}
