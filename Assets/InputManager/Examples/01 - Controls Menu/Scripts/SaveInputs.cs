﻿using UnityEngine;
using UnityEngine.Serialization;

namespace TeamUtility.IO.Examples
{
	public class SaveInputs : MonoBehaviour 
	{
		[SerializeField]
		[FormerlySerializedAs("m_exampleID")]
		private int _exampleID;

		public void Save()
		{
			print ("test");
			string saveFolder = PathUtility.GetInputSaveFolder(_exampleID);
			if(!System.IO.Directory.Exists(saveFolder))
				System.IO.Directory.CreateDirectory(saveFolder);

			InputSaverXML saver = new InputSaverXML(saveFolder + "/input_config2.xml");
			InputManager2.Save(saver);
		}
	}
}